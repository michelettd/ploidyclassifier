#!/usr/local/bin/python3.6
# encoding: utf-8
"""
 -- Identify and genotype triploid from Illumina infinium

@author:     michelettd

@copyright:  2019 FEM. All rights reserved.

@license:    GPL


@deffield    updated: Updated
"""

import argparse
import gzip
import json
# import lmfit
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import numpy as np
import pandas as pd
import pickle
import os
import re
import sys

from __init__ import version
from collections import Counter
from lmfit.models import LorentzianModel
# from textwrap import dedent
import multiprocessing as mp

global log_file
log_file = open('triploid_caller.log', 'w+')


def get_opt():
    """
    Parse the arguments from the command line.

    Returns:
        ::Args: NameSpace containing all the arguments and options to perform the analysis
    """
    try:
        # Setup argument parser
        parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-i', '--input', type=str, nargs='+', help='Final report(s) from GenomeStudio')
        parser.add_argument('-o', '--output', type=str, default='triplo_pippo', help='Output File basename ')
        parser.add_argument('-b', '--ar_bname', type=str, default='tmp',
                            help='Basename to prepernd to the Numpy memorymap arrays and temp files')
        group.add_argument('-u', '--use_precomputed_arrays', action='store_true',  # nargs='+',
                           help='Skip the array creation from final report(s) and get arrays names from ar_bname.')
        parser.add_argument('--plot', action='store_true',
                            help='Create the plots with the distibution of the B allele frequency in the "plots"'
                                 ' directory. WARNING: One plot for each individual.')
        parser.add_argument('-l', '--snp-list', type=str, help='File with the SNPs to use. One SNP per line')
        parser.add_argument('-s', '--iid-list', type=str,
                            help='File with the list of individuals to analyse. '
                                 'If iid-list is set to "debug" only 3 individuals are used and if it is not set (None)'
                                 ' all the individuals are classiefied')
        parser.add_argument('-p', '--pedigree', action='store_true',
                            help='For each triploid individual check if any diploid '
                                 'accession is compatible with parent-offspring relations')
        parser.add_argument('-t', '--threads', default=mp.cpu_count()-1, type=int,
                            help='number of threads to use [deprecated]')
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
        if len(sys.argv) == 1:
            parser.print_help()
            sys.exit(1)
        else:
            return parser.parse_args()

    except Exception as e:
        print('\nUnexpected error. Read the help\nErrorType:', e)
        return 2

# Static Methods

def get_alleles(alleles):
    """
       Starting from a dictionary of sets creates for each SNP a tupla with the two alleles to have the TOP allele first
       (('A' or 'T'), ('B' or 'C' or 'G'))
    :param
        ::alleles : dictionary of sets with all the alleles for each SNP
    :return
        ::snp_alleles: Dictionary of tuples in which keys are SNP Names and value the 2 alleles for the SNP
    """
    snp_alleles = {}
    for k, v in alleles.items():
        a1, a2 = None, None
        for a in v:
            if a in ('A', 'T'):
                a1 = a
            elif a in ('B', 'C', 'G'):
                a2 = a
            elif a == '-':
                pass
            else:
                global log_file
                print(k, v, a, 'get_alleles', file=log_file)
        snp_alleles[k] = (a1, a2)
    return snp_alleles


def get_array_shape(fin):
    """
    Array shape from the header of the Final Report from Genome Studio
    TODO: add check for the maximux number of line to read in the FinalReport Header
    :param fin: Input File (Final Report from Genome Studio
    :return:
        ::no_of_snps: No. of SNPs in Final Report
        ::no_of_ind: No. of idividuals in Final Report
    """
    no_of_snps, no_of_ind = None, None
    for line in my_open(fin):
        if 'Num SNPs' in line:
            no_of_snps = int(line.strip().split()[-1])
        elif 'Num Samples' in line:
            no_of_ind = int(line.strip().split()[-1])
        if no_of_snps and no_of_ind:
            return no_of_snps, no_of_ind


def get_snp_arr(iid_ix, data, slic):
    """

    :param iid_ix:
    :param data:
    :param slic:
    :return:
    """
    try:
        return np.array([a for a, b in zip(data[3, iid_ix, slic], data[0, iid_ix, slic])
                         if b == 1 and np.isfinite(a)])
    except:
        global log_file
        print("####Probable malformed array", file=log_file)
        print(iid_ix, type(iid_ix), slic, type(slic), data[3].shape, data[0].shape, file=log_file)
        print(data[3, iid_ix, slic], data[0, iid_ix, slic], file=log_file)
        # raise
        return np.array([])


def gts2num(gts):
    # get the 2 alleles
    gts = np.array(gts)
    numgts = np.zeros(gts.shape)
    # print(gts.shape)
    for i in range(gts.shape[0]):
        if gts[i] in ('AA', 'TT'):
            continue
        elif gts[i] in ('AC', 'AG', 'AB'):
            numgts[i] = 1
        elif gts[i] in ('CC', 'GG', 'BB'):
            numgts[i] = 2
        else:
            numgts[i] = np.nan
    return numgts


def make_model(components):
    if components == 'unimodal':
        mod1 = LorentzianModel(prefix='l1_', nan_policy='propagate')
        mod1.param_hints.pop('fwhm')
        mod1.param_hints.pop('height')

        pars = mod1.make_params()
        pars['l1_center'].set(value=0.5, min=0.45, max=0.55)
        pars['l1_sigma'].set(value=0.04, min=0.02, max=0.1)
        pars['l1_amplitude'].set(value=1., min=0.2, max=2)
        return mod1, pars
    elif components == 'bimodal':
        mod1 = LorentzianModel(prefix='l1_', nan_policy='propagate')
        mod2 = LorentzianModel(prefix='l2_', nan_policy='propagate')
        mod1.param_hints.pop('fwhm')
        mod1.param_hints.pop('height')
        mod2.param_hints.pop('fwhm')
        mod2.param_hints.pop('height')
        mc = mod1+mod2
        pars = mc.make_params()
        pars['l1_center'].set(value=0.33, min=0.3, max=0.43, brute_step=.01)
        pars['l1_sigma'].set(value=0.02, min=0.01, max=0.1, brute_step=.01)
        pars['l1_amplitude'].set(value=1, min=0.2, max=2, brute_step=.01)
        pars['l2_center'].set(value=0.66, min=0.57, max=0.7, brute_step=.01)
        pars['l2_sigma'].set(value=0.02, min=0.01, max=0.1, brute_step=.01)
        pars['l2_amplitude'].set(value=1, min=0.2, max=2, brute_step=.01)
        return mc, pars
    if components == 'trimodal':
        mod1 = LorentzianModel(prefix='l1_', nan_policy='propagate')
        mod2 = LorentzianModel(prefix='l2_', nan_policy='propagate')
        mod3 = LorentzianModel(prefix='l3_', nan_policy='propagate')
        mod1.param_hints.pop('fwhm')
        mod1.param_hints.pop('height')
        mod2.param_hints.pop('fwhm')
        mod2.param_hints.pop('height')
        mod3.param_hints.pop('fwhm')
        mod3.param_hints.pop('height')
        mc = mod1+mod2+mod3
        pars = mc.make_params()
        pars['l1_center'].set(value=0.3, min=0.25, max=0.4, brute_step=.01)
        pars['l1_sigma'].set(value=0.02, min=0.01, max=0.1, brute_step=.01)
        pars['l1_amplitude'].set(value=1, min=0.2, max=2, brute_step=.1)
        pars['l2_center'].set(value=0.5, min=0.4, max=0.6, brute_step=.01)
        pars['l2_sigma'].set(value=0.02, min=0.01, max=0.1, brute_step=.01)
        pars['l2_amplitude'].set(value=1, min=0.2, max=2, brute_step=.1)
        pars['l3_center'].set(value=0.7, min=0.6, max=0.8, brute_step=.01)
        pars['l3_sigma'].set(value=0.02, min=0.01, max=0.1, brute_step=.01)
        pars['l3_amplitude'].set(value=1, min=0.2, max=2, brute_step=.01)
        # print(pars)
        return mc, pars


def my_open(fin):
    if fin[-3:] == '.gz':
        return gzip.open(fin, 'rt')
    else:
        return open(fin)


def num2gt(num_gts, snp_order, alleles):
    gts = []
    for g, ix in zip(num_gts, snp_order):
        if np.isnan(g):
            gts.append('--')
        elif g == 0:
            gts.append(alleles[ix][0] * 2)
        elif g == 1:
            gts.append(alleles[ix][0] + alleles[ix][1])
        elif g == 2:
            gts.append(alleles[ix][1] * 2)
    return np.array(gts)


def plot_ploidy(x, y, dens_sc, dmax_sc, maxima_sc, iid):
    _, ax = plt.subplots()  # 2,1, sharex=True)
    ax.hist(y, 100, density=True)
    ax.plot(x, dens_sc, 'r-', label='scipy')
    ax.plot(maxima_sc, dens_sc[dmax_sc], 'ro')
    plt.legend()
    if not os.path.isdir(os.path.join(os.getcwd(), 'ploidy_plots')):
        os.makedirs(os.path.join(os.getcwd(), 'ploidy_plots'))
    fname = os.path.join(os.getcwd(), 'ploidy_plots', re.sub(r"[/\\'() ]", '', iid) + '.png')
    plt.savefig(fname)
    plt.close()


def regenotype(gt, theta, median_ab, a1, a2):
    if gt == 1:
        if theta > median_ab:
            return a1 + a2 + a2
        else:
            return a1 + a1 + a2
    elif gt == 0:
        return a1 + a1 + a1
    elif gt == 2:
        return a2 + a2 + a2
    elif np.isnan(gt):
        return '---'


def trip_gts2num(gts):
    ngts = np.copy(gts)
    #print(ngts)
    ngts[gts == 'AAA'] = 0.
    ngts[gts == 'TTT'] = 0.
    ngts[gts == 'AAC'] = 1.
    ngts[gts == 'AAG'] = 1.
    ngts[gts == 'AAB'] = 1.
    ngts[gts == 'ACC'] = 2.
    ngts[gts == 'AGG'] = 2.
    ngts[gts == 'ABB'] = 2.
    ngts[gts == 'CCC'] = 3.
    ngts[gts == 'GGG'] = 3.
    ngts[gts == 'BBB'] = 3.
    ngts[gts == '---'] = np.nan
    #print(ngts)
    return ngts.astype(float)


# Classes


class GetDataFast(object):
    def __init__(self, use_precomputed_arrays, ar_bname, fnames=None):
        """
        Class that loads the data from Final Report files or from memory
            mapped array used in previous runs of the sofware.
        :param use_precomputed_arrays: A boolean that specify the input to use
        :param ar_bname: basename of the memory mapped array to create or to read
        :param fnames: List of final reports to parse
        """
        self.bname = ar_bname
        # print(self.bname)
        if use_precomputed_arrays:
            self.samples_order = pickle.load(open('{}_samples_order'.format(self.bname), 'rb'))
            self.snp_order = pickle.load(open('{}_snps_order'.format(self.bname), 'rb'))
            self.snp_alleles = pickle.load(open('{}_snp_alleles'.format(self.bname), 'rb'))
            self.het_count = pickle.load(open('{}_het_count'.format(self.bname), 'rb'))
            self.data_mat = np.memmap('{}_mmap.dat'.format(self.bname), mode='r+',
                                      shape=(4, len(self.samples_order), len(self.snp_order)), dtype=float)
            self.het_ball = np.memmap('{}_ballH.dat'.format(self.bname), mode='r+',
                                      shape=(len(self.samples_order), len(self.snp_order)), dtype=float)
        else:
            self.fnames = fnames
            if not self.fnames:
                global log_file
                print('GetData.__init__', 'Invalid file name list', file=log_file)
                # print('Invalid file name list')
                sys.exit()
            self.cln_idx, self.cln_names = self.get_cols_idx()
            self.a1_ix = self.cln_idx[self.cln_names.index('Allele1')]
            self.a2_ix = self.cln_idx[self.cln_names.index('Allele2')]
            self.theta_ix = self.cln_idx[self.cln_names.index('Theta')]
            self.r_ix = self.cln_idx[self.cln_names.index('R')]
            self.ball_ix = self.cln_idx[self.cln_names.index('BAlleleFreq')]
            self.iid_ix = self.cln_idx[self.cln_names.index('SampleID')]
            self.sid_ix = self.cln_idx[self.cln_names.index('SNPName')]
            self.snp_order, self.samples_order, self.data_mat, self.snp_alleles, self.het_count, self.het_ball = \
                self.finalreport_to_array(bname=self.bname)
            pickle.dump(self.snp_alleles, open('{}_snp_alleles'.format(self.bname), 'wb'))
            pickle.dump(self.samples_order, open('{}_samples_order'.format(self.bname), 'wb'))
            pickle.dump(self.snp_order, open('{}_snps_order'.format(self.bname), 'wb'))
            pickle.dump(self.het_count, open('{}_het_count'.format(self.bname), 'wb'))
        self.samples_order_dict = dict((k, i) for i, k in enumerate(self.samples_order))

    def get_cols_idx(self):
        """
        Read the Final report to get the index and name of the needed columns
        :return:
            cls_idx: columns indexes
            cls_names: columns names without spaces
        """
        cls_idx, cls_names = [], []
        for line in my_open(self.fnames[0]):
            if 'SNP Name' in line:
                for i, el in enumerate(line.strip().split('\t')):
                    if el == 'SNP Name':
                        cls_idx.append(i)
                        cls_names.append(el.replace(' ', ''))
                    elif el == 'Sample ID':
                        cls_idx.append(i)
                        cls_names.append(el.replace(' ', ''))
                    elif 'Allele1' in el:
                        cls_idx.append(i)
                        cls_names.append('Allele1')
                    elif 'Allele2' in el:
                        cls_idx.append(i)
                        cls_names.append('Allele2')
                    elif el == 'Theta':
                        cls_idx.append(i)
                        cls_names.append(el)
                    elif el == 'R':
                        cls_idx.append(i)
                        cls_names.append(el)
                    elif el == 'B Allele Freq':
                        cls_idx.append(i)
                        cls_names.append(el.replace(' ', ''))
                return cls_idx, cls_names

    def finalreport_to_array(self, path=os.getcwd(), bname='tmp'):
        """
        Read the Final Report and load the data in memmap array
        :param path: path where memmap is saved. Default: current working directory
        :param bname: memmap basename. Default: tmp
        :return snp_ids: dictionary with SNP name as key and SNP index in memmap of the SNP
        :return sample_ids: list of samples to analyze
        :return mymmap: memmap array with shape=(4, no_of_ind, no_of_snps) the four layers are  gt, theta, r, and ball
        :return snp_alleles: dictionary of tuple with the allels for each SNP
        :return het_count: dictionaty with the number of heterozigote individuals for each SNP
        :return myball: memmap array with shape=(no_of_ind, no_of_snps) with the B Allele Frequency of the heterozygotes
        """
        global log_file
        no_of_snps, no_of_ind = get_array_shape(self.fnames[0])
        mymmap = np.memmap(os.path.join(path, '{}_mmap.dat'.format(bname)), mode='w+',
                           shape=(4, no_of_ind, no_of_snps), dtype=float)
        # layers = ['numgt_mat', 'theta_mat', 'r_mat', 'ball_mat']
        myball = np.memmap(os.path.join(path, '{}_ballH.dat'.format(bname)), mode='w+',
                           shape=(no_of_ind, no_of_snps), dtype=float)
        sample_ids, snp_ids, passed_id, het_count = [], {}, set(), {}

        def fill_mmap():
            myball[arr_coor] = np.concatenate((het_ball_arr, np.full(no_of_snps - het_ball_arr.shape[0], np.nan)))
            mymmap[0, arr_coor] = tmp_gt  # Convert to number
            mymmap[1, arr_coor] = tmp_theta
            mymmap[2, arr_coor] = tmp_r
            mymmap[3, arr_coor] = tmp_ball
        first_file = True
        alleles = {}
        for fin in self.fnames:
            start, prev = False, None
            tmp_gt, tmp_theta, tmp_r, tmp_ball = [], [], [], []
            for i, line in enumerate(my_open(fin)):
                if not start and 'SNP Name' in line:
                    start = True
                    continue
                elif start:
                    if i % 1e6 == 0:
                        print(fin, i, file=log_file)
                    line = line.rstrip('\r\n').split('\t')
                    try:
                        if first_file:
                            try:
                                snp_ids[line[self.sid_ix]]
                            except KeyError:
                                snp_ids[line[self.sid_ix]] = len(snp_ids)
                        if not line[self.iid_ix] in passed_id:
                            passed_id.add(line[self.iid_ix])
                            sample_ids.append(line[self.iid_ix])
                    except IndexError:
                        pass
                    try:
                        alleles[line[self.sid_ix]].add(line[self.a1_ix])
                        alleles[line[self.sid_ix]].add(line[self.a2_ix])
                    except KeyError:
                        alleles[line[self.sid_ix]] = {line[self.a1_ix], line[self.a2_ix]}
                    if prev is None or line[self.iid_ix] == prev:
                        prev = line[self.iid_ix]
                        tmp_gt.append(line[self.a1_ix] + line[self.a2_ix])
                        tmp_theta.append(line[self.theta_ix])
                        tmp_r.append(line[self.r_ix])
                        tmp_ball.append(line[self.ball_ix])
                    else:
                        arr_coor = sample_ids.index(prev)
                        tmp_gt = gts2num(tmp_gt)
                        het_ball_arr = np.array(tmp_ball, dtype=float)[tmp_gt == 1]
                        het_count[prev] = len(het_ball_arr)
                        fill_mmap()
                        prev = line[self.iid_ix]
                        tmp_gt, tmp_theta = [line[self.a1_ix] + line[self.a2_ix]], [line[self.theta_ix]]
                        tmp_r, tmp_ball = [line[self.r_ix]], [line[self.ball_ix]]
            arr_coor = sample_ids.index(line[self.iid_ix])
            tmp_gt = gts2num(tmp_gt)
            het_ball_arr = np.array(tmp_ball, dtype=float)[tmp_gt == 1]
            het_count[line[self.iid_ix]] = len(het_ball_arr)
            fill_mmap()
            first_file = False
        snp_alleles = get_alleles(alleles)
        snp_ids = [k for k, v in sorted(snp_ids.items(), key=lambda x: x[1])]
        return snp_ids, sample_ids, mymmap, snp_alleles, het_count, myball


class PloidyCalculator(object):
    def __init__(self, snp_order, samples_order, data_mat, het_count):
        """
        Infer the ploidy of each individual passed by fitting of a model
            with one, two or three gaussian on the histogram of the
            B Allele Frequency of the heterozygous SNPs.
        :param snp_order: list with the order of the SNPs to parse
        :param samples_order: list with the order of the Samples to parse
        :param data_mat: memmap array with the data shape=(4, no_of_ind, no_of_snps) the four layers are  gt, theta,
                         r, and ball
        :param het_count: dictionaty with the number of heterozigote individuals for each SNP
        """
        self.snp_order = snp_order
        self.samples_order = samples_order
        self.mydata = data_mat
        self.het_count = het_count
        self.ploidy = {}

    def get_ploidy_lmfit_multiproc(self, sample2parse=('1_17_james_grieve', '1_1_co-op_36', '1_7_fiamma'),
                                   snp2parse=None, concurrency=mp.cpu_count()-1):
        """
        Apply lmfit using multithreding
        #Deprecated because if to FIX
        :param sample2parse: list of samples to prse
        :param snp2parse: list of SNPs to parse
        :param concurrency: No. of concurrent process
        :return ploidy: dictionary with the ploidy level for each sample
        """
        # sample2parse=('1_17_james_grieve', '1_1_co-op_36', '1_7_fiamma')
        global log_file
        if snp2parse:
            sidx = np.array([i for i, sid in enumerate(self.snp_order) if sid in snp2parse])
        else:
            sidx = slice(len(self.snp_order))
        (umodel, upar) = make_model('unimodal')
        (bmodel, bpar) = make_model('bimodal')
        (tmodel, tpar) = make_model('trimodal')
        mddict = {'u': ((umodel, upar),2), 'b': ((bmodel, bpar),3), 't': ((tmodel, tpar),4)}

        manager = mp.Manager().dict()

        def fit_model(model, param, x, y, sid, mn, sema):
            res_point = model.fit(y, param, x=x)
            manager[(sid, mn)] = res_point.dumps()
            sema.release()
            return

        processes = []
        sema = mp.Semaphore(concurrency)
        for iid in sample2parse:
            try:
                ball = self.mydata[self.samples_order.index(iid),:self.het_count[iid]]
                print(iid, file=log_file)
                if len(ball) == 0:
                    self.ploidy[iid] = np.nan
                    continue
                yv, xv = np.histogram(ball, 100, density=True)
                xv = np.array((xv[1:] + xv[:-1]) / 2)  # for len(x)==len(y)
                idx = np.isfinite(yv)
                for mname, ((mvar, mpar), _) in mddict.items():
                    sema.acquire()
                    process = mp.Process(target=fit_model, args=(mvar, mpar, xv[idx], yv[idx], iid, mname, sema))
                    process.start()
                    processes.append(process)
            except:
                # print(iid,plo, gmodelu, paru, resu.fit_report())#self.ploidy)
                # print(iid,plo, gmodelb, parb, resb.fit_report())#self.ploidy)
                # print(iid,plo, gmodelt, part, rest.fit_report())#self.ploidy)
                print('except', iid, 'No B allele freq data', yv, xv, idx, file=log_file)
                raise
        for process in processes:
            process.join()
        for iid in sample2parse:
            plo = [1e9, 2, 0]
            for m, (_, pl) in mddict.items():
                modres = json.loads(manager[(iid, m)])
                bic = modres['bic']
                best_val = modres['best_values']
                if m == 'u':
                    a_ratio, l3_center = 0, 0.2
                elif m == 'b':
                    a_ratio = (max(best_val['l1_amplitude'], best_val['l2_amplitude']) /
                               min(best_val['l1_amplitude'], best_val['l2_amplitude']))
                    l3_center = 0.2
                elif m == 't':
                    a_ratio = (max(best_val['l1_amplitude'], best_val['l2_amplitude'], best_val['l3_amplitude']) /
                               min(best_val['l1_amplitude'], best_val['l2_amplitude'], best_val['l3_amplitude']))
                    l3_center = best_val['l3_center']
                if bic < plo[0] and a_ratio < 3 and l3_center != 0.5:
                    plo = [bic, pl, a_ratio]
            self.ploidy[iid] = plo[1]
        return self.ploidy

    def get_ploidy_lmfit(self, sample2parse=('1_17_james_grieve', '1_1_co-op_36', '1_7_fiamma'),
                         snp2parse=None):
        """
        Apply lmfit models as defined by make model. For each sample 3 models are tested (unimodal, bimoodal and
        trimodal)
        #TODO speed up fitting of trimodal model
        :param sample2parse: list of samples to prse
        :param snp2parse: list of SNPs to parse

        :return ploidy: dictionary with the ploidy level for each sample
        """
        global log_file
        if snp2parse:
            sidx = np.array([i for i, sid in enumerate(self.snp_order) if sid in snp2parse])
        else:
            sidx = slice(len(self.snp_order))
        (umodel, upar) = make_model('unimodal')
        (bmodel, bpar) = make_model('bimodal')
        (tmodel, tpar) = make_model('trimodal')
        for iid in sample2parse:
            try:
                ball = get_snp_arr(self.samples_order.index(iid), self.mydata, sidx)
                print(iid, file=log_file)
                if len(ball) == 0:
                    self.ploidy[iid] = np.nan
                    continue
                yv, xv = np.histogram(ball, 100, density=True)
                xv = np.array((xv[1:] + xv[:-1]) / 2)  # for len(x)==len(y)
                idx = np.isfinite(yv)
                plo = [1e9, 2, 0]
                res = umodel.fit(yv[idx], upar, x=xv[idx])
                if res.bic < plo[0]:
                    plo = [res.bic, 2, 1]
                res = bmodel.fit(yv[idx], bpar, x=xv[idx])
                max_a = max(res.best_values['l1_amplitude'], res.best_values['l2_amplitude'])
                min_a = min(res.best_values['l1_amplitude'], res.best_values['l2_amplitude'])
                if res.bic < plo[0] and max_a/min_a < 3:
                    plo = [res.bic, 3, max_a/min_a]
                res = tmodel.fit(yv[idx], tpar, x=xv[idx])
                max_a = max(res.best_values['l1_amplitude'], res.best_values['l2_amplitude'],
                            res.best_values['l3_amplitude'])
                min_a = min(res.best_values['l1_amplitude'], res.best_values['l2_amplitude'],
                            res.best_values['l3_amplitude'])
                if (res.bic < plo[0] and max_a/min_a < 3 and
                        res.best_values['l3_center'] != 0.5):
                    plo = [res.bic, 4, max_a/min_a]
                self.ploidy[iid] = plo[1]

            except:
                # print(iid,plo, gmodelu, paru, resu.fit_report())#self.ploidy)
                # print(iid,plo, gmodelb, parb, resb.fit_report())#self.ploidy)
                # print(iid,plo, gmodelt, part, rest.fit_report())#self.ploidy)
                print('except', iid, 'No B allele freq data', yv, xv, idx, file=log_file)
                raise
                
        return self.ploidy


class TriploidRecall(object):
    def __init__(self, ploidy, samples_order_dict, snp_order, data_mat, alleles):
        """
        Regenotype triploid samples accounting for A/B dosage
        :param ploidy: dictionary with the ploidy level of all the samples
        :param samples_order_dict: dictionary with the sample order (key = sample, value=index)
        :param snp_order: list with the order of the SNPs
        :param data_mat: memmap with the data
        :param alleles: dictinary with the alleles for each SNP
        """
        self.ploidy = ploidy
        self.snp_order = snp_order
        self.mydata = data_mat
        self.snp_alleles = alleles
        self.trip_order = tuple(k for (k, v) in self.ploidy.items() if v == 3)
        self.trip_order = dict((k, i) for i, k in enumerate(self.trip_order))
        self.trigt = np.full((len(self.trip_order), len(self.snp_order)), '---')
        self.samples_order_dict = samples_order_dict

    def iterator(self, snp_subset):
        """
        Iterate over SNPs to determine the genotype of triploid individuals.
        The heterozygote dosge is determined using the median value of Theta.
        genotype == AAB if Theta < median_ab
        genotype == ABB if Theta > median_ab
        For detail see regenotype function
            :param snp_subset: Subset of SNPs to parse
            :return trigt: genotypes of triploid individual (np.full((len(self.trip_order), len(self.snp_order)), '---')
            :return trip_order: dictionary with the order of triploid individuals in trigt {'sample_id': index}
        """
        tri_idx = [self.samples_order_dict[iid] for iid, pl in self.ploidy.items() if pl == 3]
        tri_id = [iid for iid, pl in self.ploidy.items() if pl == 3]
        mask_ab = self.mydata[0] == 1
        for i, snpid in enumerate(self.snp_order):
            if snpid in snp_subset:
                try:
                    try:
                        # get the 2 alleles. If not possible skip the SNP
                        (a1, a2) = self.snp_alleles[snpid]

                    except IndexError:
                        continue
                    if not a1 or not a2:
                        continue

                    median_ab = np.median(self.mydata[1, :, i][mask_ab[:, i]], axis=0)
                    gt, theta = self.mydata[0, tri_idx, i], self.mydata[1, tri_idx, i]
                    for ix in range(len(tri_idx)):
                        self.trigt[self.trip_order[tri_id[ix]], i] = regenotype(gt[ix], theta[ix], median_ab, a1, a2)
                except:
                    raise
        return self.trigt, self.trip_order


class TriploidPedigree(object):
    def __init__(self,trip_order, trigt, ploidy, samples_order_dict, data_mat, snp_alleles):
        """
        Infere for ezch triploid inndividual possible first degree relationship both unreduced gamete or diploid paret
        :param trip_order: dictionary with the order of triploid individuals in trigt {'sample_id': index}
        :param trigt: genotypes of triploid individual (np.full((len(self.trip_order), len(self.snp_order)), '---')
        :param ploidy: ploidy level of aech individual
        :param samples_order_dict:
        :param data_mat:
        :param snp_alleles:
        """
        self.trip_order = trip_order
        self.ploidy = ploidy
        self.samples_order_dict = samples_order_dict
        self.trigt = trigt
        self.data_mat = data_mat
        self.alleles = snp_alleles
        self.relationship = dict((tiid, {'unreduced': [], 'dip_par': []}) for tiid in self.trip_order)

    def iterator(self, snp_order=None, snp2parse=None, min_sim=0.99):
        """
        Diploid and triploid genotypes are converted to numeric counting the number of alternative alleles.
        For the diploids the possible values are: 0 (AA), 1 (AB), and 2 (BB). For triploids the possible values are:
        0 (AAA), 1 (AAB), 2 (ABB) and 3 (BBB). The number of common alles between diploid and triploid is calculated
        subtracting the diploid genotipe to the triploid. The possible values of the subtraction are:
            0    1    2
        0   0   -1   -2
        1   1    0   -1
        2   2    1    0
        3   3    2    1
        -2 and 3 are due to no alleles in common (AAA-BB and BBB-AA)
        -1 and 2 are due to one common allele (AAA-AB and AAB-BB). These values are contributing to diploid parent
        0 and 1 are due to two common alleles (AAA-AA, AAB-AB, ABB-BB, AAB-AA, ABB-AB and BBB-BB). hese values are
            contributing both to diploid parent and unreduced gamete.

        :param snp_order: list containing the order of the SNPs in the data matrix
        :param snp2parse: list of SNPs to parse
        :param min_sim: threshold of common snp to define the relation as real the default is 99% of compatible
         triploid-diploid genotype combination. (defualt: 0.99)
        :return:
            :relationship: dictionary with the detected relationship.
        """
        if snp2parse:
            sidx = np.array([i for i, sid in enumerate(snp_order) if sid in snp2parse])
        else:
            sidx = slice(len(snp_order))
        diplo = [iid for iid, pl in self.ploidy.items() if pl == 2]
        diplo_ix = [self.samples_order_dict[iid] for iid, pl in self.ploidy.items() if pl == 2]

        ntgt = trip_gts2num(self.trigt[:,sidx])
        for dix, iid in zip(diplo_ix, diplo):
            #print(ntgt.shape, dix,sidx, self.data_mat[0, dix, sidx].shape)
            dosage_diff_full = ntgt - self.data_mat[0, dix, sidx]
            ma_full = np.isfinite(dosage_diff_full)
            for tiid, ix in self.trip_order.items():
                dosage_diff = dosage_diff_full[ix]
                ma = ma_full[ix]
                cnt = Counter(dosage_diff[ma])
                comp = sum(ma)
                unreduced = cnt[0] + cnt[1]
                dip_par = unreduced + cnt[-1] + cnt[2]
                ure_ratio, dip_ratio = unreduced / comp, dip_par / comp
                if ure_ratio > min_sim:  # rel == 'unreduced':
                    self.relationship[tiid]['unreduced'].append((iid, ure_ratio))
                elif dip_ratio > min_sim:  # rel == 'dip_par':
                    self.relationship[tiid]['dip_par'].append((iid, dip_ratio))
        return self.relationship


class Exporter(object):
    def __init__(self, fout_bname, snp2parse, ploidy, trigt, trip_order, snp_order):
        # super(Exporter, self).__init__(use_precomputed_arrays, ar_bname, fnames)#trigt, trip_order, ploidy, fname,
        # snp_order, snp2parse):
        self.ploidy = ploidy
        self.trigt = trigt
        self.trip_order = trip_order
        self.fout = pd.ExcelWriter('{}_ploidy.xlsx'.format(fout_bname))
        self.snp_order = snp_order
        self.snp2parse = np.array([i for i, a in enumerate(self.snp_order) if a in snp2parse])

    def ploidy_classif(self, relationship):
        tmp_ploidy = pd.DataFrame(columns=['iid', 'ploidy', 'No. of AAA', 'No. of AAB', 'No. of ABB', 'No. of BBB',
                                           'No. of NoCall', 'Unreduced gamete', 'Normal gamete'],
                                  index=range(len(self.ploidy)))

        for i, (iid, pl) in enumerate(self.ploidy.items()):
            row = [iid, pl]
            if pl == 3:
                tix = self.trip_order[iid]
                call = 0
                no_of_a = np.array([np.nan if el == '---' else Counter(el)['A'] for el in self.trigt[tix, self.snp2parse]])
                for gt in (3, 2, 1, 0):  # Number of A
                    cnt = len(no_of_a[no_of_a == gt])
                    call += cnt
                    row.append(cnt)
                row.append(len(self.trigt[tix, self.snp2parse]) - call)
                if not relationship:
                    row.extend(['None', 'None'])
                else:
                    tmp = []
                    try:
                        tmp.append(','.join([str(a)+':'+str(b) for a,b in relationship[iid]['unreduced']]))
                    except KeyError:
                        tmp.append('None')
                    try:
                        tmp.append(','.join([str(a)+':'+str(b) for a,b in relationship[iid]['dip_par']]))
                    except KeyError:
                        tmp.append('None')
                    row.extend(tmp)
            else:
                row.extend(['-', '-', '-', '-', '-', 'None', 'None'])

            tmp_ploidy.loc[i] = row
        tmp_ploidy.to_excel(self.fout, sheet_name='ploidy classification')

    def triploid_gts(self):
        tmp_gts = pd.DataFrame(self.trigt[:, self.snp2parse],
                               index=[k for k, _ in sorted(self.trip_order.items(), key=lambda x: x[1])],
                               columns=np.array(self.snp_order)[self.snp2parse])
        tmp_gts = tmp_gts.transpose()
        tmp_gts.to_excel(self.fout, sheet_name='triploid_genotypes')
        self.fout.close()


class UiPlotter(object):
    def __init__(self, snp_order, samples_order, data_mat):
        self.snp_order = snp_order
        self.samples_order = samples_order
        self.data_mat = data_mat

    def plot_ploidy(self, iid, ploidy_iid, canvas, snp2parse=None):
        if snp2parse:
            sidx = np.array([i for i, sid in enumerate(self.snp_order) if sid in snp2parse])
        else:
            sidx = slice(len(self.snp_order))
        iid_ix = self.samples_order.index(iid)
        ball = get_snp_arr(iid_ix, self.data_mat, sidx)
        yv, xv = np.histogram(ball, 100, density=True)
        xv = np.array((xv[1:]+xv[:-1])/2) # for len(x)==len(y)
        idx = np.isfinite(yv)
        if ploidy_iid == '2':
            (gmodel, par) = make_model('unimodal')
        elif ploidy_iid == '3':
            (gmodel, par) = make_model('bimodal')
        elif ploidy_iid == '4':
            (gmodel, par) = make_model('trimodal')
        else:
            (gmodel, par) = make_model('unimodal')
        res = gmodel.fit(yv[idx], par, x=xv[idx])
        canvas.ax.clear()
        canvas.ax.set_xlim(0.1, 0.9)
        canvas.ax.set_title(iid, fontdict={'fontsize': 10})
        canvas.ax.text(0.7, np.nanmax(yv)*0.9, 'Ploidy {0}\nHet SNPs {1}'.format(ploidy_iid, len(ball)),
                       bbox=dict(facecolor='red', alpha=0.2), fontsize=8)
        canvas.ax.set_xlabel('B allele freq')
        canvas.ax.set_ylabel('density')
        canvas.ax.bar(xv, height=yv, width=(xv[1]-xv[0]))
        canvas.ax.plot(xv, res.best_fit, 'r-')
        canvas.fig.tight_layout()
        canvas.draw()
        return canvas

    def plot_gslike(self, snpid, canvas, trip_order, trigt,
                    snp_alleles, iid2parse=None):
        if iid2parse:
            slic = np.array([i for i, sid in enumerate(self.samples_order) if sid in iid2parse])
            iid2parse = np.array([sid for sid in self.samples_order if sid in iid2parse])
        else:
            slic = slice(len(self.samples_order))
            iid2parse = self.samples_order
        sidx = self.snp_order.index(snpid)
        # Data to plot
        mydata = self.data_mat[:3, slic, sidx]
        cols = []
        try:
            # get the 2 alleles
            (a1, a2) = snp_alleles[snpid]
            if not a1 or not a2:
                median_ab = 0.5
            else:
                mask_ab = mydata[0] == 1
                median_ab = np.median(mydata[1][mask_ab[:, sidx]], axis=0)
        except IndexError:
            median_ab = 0.5
        for i, iid in enumerate(iid2parse):

            if np.isnan(mydata[0, i]):  # Missing data in diploid
                cols.append('grey')
            elif iid in trip_order:  # set triploids gt and color
                iidx = trip_order[iid]
                tgt = trigt[iidx, sidx]
                if tgt == '---':
                    cols.append('grey')
                elif len(set(tgt)) == 1:
                    if mydata[1, i] < median_ab:
                        cols.append('orange')
                    else:
                        cols.append('k')
                elif len(set(tgt)) == 2:
                    if tgt[0] == tgt[1]:  # AAB
                        cols.append('g')
                    elif tgt[1] == tgt[2]:  # ABB
                        cols.append('c')
                    else:
                        global log_file
                        print('else', tgt, iid, snpid, file=log_file)
            else:  # set diploids colors
                gt = mydata[0, i]
                if gt == 0:
                    cols.append('b')
                elif gt == 1:
                    cols.append('m')
                else:   
                    cols.append('r')
        canvas.ax.clear()
        pltitle = snpid
        if len(pltitle) > 30:
            pltitle = pltitle[:20]+'...'+pltitle[-10:]
        canvas.ax.set_title(pltitle, fontdict={'fontsize': 10})
        canvas.ax.set_xlabel('Theta')
        canvas.ax.set_ylabel('R')
        maxy = np.nanmax(mydata[2, :])
        if not np.isfinite(maxy):
            maxy = 3
        canvas.ax.set_ylim(0, maxy*1.3)
        canvas.ax.set_xlim(0, 1)
        canvas.ax.grid(True)
        canvas.ax.scatter(mydata[1, :], mydata[2, :], c=cols, s=6)
        handles, labels = [], ['NoCall', 'AA', 'AB', 'BB', 'AAA', 'AAB', 'ABB', 'BBB']
        for i, h in enumerate(('grey', 'b', 'm', 'r', 'orange', 'g', 'c', 'k')):
            handles.append(mpatches.Patch(color=h, label=labels[i]))
        canvas.ax.legend(handles=handles, loc='upper right',
                         bbox_to_anchor=[0.95, 0.99], ncol=4, 
                         borderaxespad=0., prop={'size': 6})
        canvas.fig.tight_layout()
        canvas.draw()
        return canvas


def main():
    args = vars(get_opt())

    # Load data from input files
    data = GetDataFast(use_precomputed_arrays=args["use_precomputed_arrays"],
                       ar_bname=args["ar_bname"],
                       fnames=args["input"])
    return
    # Make a tuple with the subset individuals to parse or make it identicalto the sample order.
    # If the debug switch is uset set iid to a tuple with 3 fixed inds
    if args["iid_list"] == 'debug':
        iid_set = ('1_17_james_grieve', '1_1_co-op_36', '1_7_fiamma')
    elif args["iid_list"]:
        print(args["iid_list"])
        iid_set = tuple(line.strip().split()[0] for line in open(args["iid_list"]) if len(line.strip().split()) > 0)
    else:
        iid_set = data.samples_order

    # Make a set with the subset SNPs to parse or inizialize the snp_subset as a set with all the SNP in the FinalReport
    if args["snp_list"]:
        snp_subset = set(line.strip().split()[0] for line in open(args["snp_list"]))
    else:
        snp_subset = set(data.snp_order)

    # Infer the ploidy level of each individual in iid_set
    print('getting ploidy')
    plclassif = PloidyCalculator(snp_order=data.snp_order,
                                 samples_order=data.samples_order,
                                 data_mat=data.data_mat,
                                 het_count=data.het_count)
    plclassif.ploidy = plclassif.get_ploidy_lmfit(sample2parse=iid_set, snp2parse=snp_subset)

    # Regenotype the triploid individuals
    tr = TriploidRecall(ploidy=plclassif.ploidy,
                        samples_order_dict=data.samples_order_dict,
                        snp_order=data.snp_order,
                        data_mat=data.data_mat,
                        alleles=data.snp_alleles)
    tr.trigt, tr.trip_order = tr.iterator(snp_subset=snp_subset)

    print(tr.trip_order.keys())

    # Make a paternity test to determine the IBD of each pair of individuals. Only if pedigree swithch activated
    tp = TriploidPedigree(trip_order=tr.trip_order,
                          trigt=tr.trigt,
                          ploidy=plclassif.ploidy,
                          samples_order_dict=data.samples_order_dict,
                          data_mat=data.data_mat,
                          snp_alleles=data.snp_alleles)
    if args["pedigree"]:
        tp.relationship = tp.iterator(snp_order=data.snp_order,
                                         snp2parse=snp_subset)#,
                                         #concurrency=args['threads'])
    else:
        tp.relationship = dict()
    print(tp.relationship)

    # Export results to files
    if args["output"]:
        outpath = args["output"]
        ee = Exporter(fout_bname=outpath,
                      snp2parse=snp_subset,
                      ploidy=plclassif.ploidy,
                      trigt=tr.trigt,
                      trip_order=tr.trip_order,
                      snp_order=data.snp_order)
        ee.ploidy_classif(tp.relationship)
        ee.triploid_gts()
    return


if __name__ == "__main__":
    main()
