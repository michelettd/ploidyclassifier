# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'triploid_class/exportDialog.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_exportDialog(object):
    def setupUi(self, exportDialog):
        exportDialog.setObjectName("exportDialog")
        exportDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        exportDialog.resize(496, 295)
        self.buttonBox = QtWidgets.QDialogButtonBox(exportDialog)
        self.buttonBox.setGeometry(QtCore.QRect(140, 230, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.outfolderSelect = QtWidgets.QPushButton(exportDialog)
        self.outfolderSelect.setGeometry(QtCore.QRect(450, 80, 31, 30))
        self.outfolderSelect.setObjectName("outfolderSelect")
        self.outfolderText = QtWidgets.QLineEdit(exportDialog)
        self.outfolderText.setGeometry(QtCore.QRect(160, 80, 291, 29))
        self.outfolderText.setObjectName("outfolderText")
        self.label_2 = QtWidgets.QLabel(exportDialog)
        self.label_2.setGeometry(QtCore.QRect(20, 80, 131, 21))
        self.label_2.setObjectName("label_2")
        self.outfileText = QtWidgets.QLineEdit(exportDialog)
        self.outfileText.setGeometry(QtCore.QRect(160, 150, 291, 29))
        self.outfileText.setObjectName("outfileText")
        self.label_3 = QtWidgets.QLabel(exportDialog)
        self.label_3.setGeometry(QtCore.QRect(20, 150, 131, 21))
        self.label_3.setObjectName("label_3")
        self.label = QtWidgets.QLabel(exportDialog)
        self.label.setGeometry(QtCore.QRect(21, 21, 301, 21))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setWordWrap(False)
        self.label.setObjectName("label")

        self.retranslateUi(exportDialog)
        self.buttonBox.accepted.connect(exportDialog.accept)
        self.buttonBox.rejected.connect(exportDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(exportDialog)

    def retranslateUi(self, exportDialog):
        _translate = QtCore.QCoreApplication.translate
        exportDialog.setWindowTitle(_translate("exportDialog", "Export Results"))
        self.outfolderSelect.setToolTip(_translate("exportDialog", "Select the Full Data Table input file"))
        self.outfolderSelect.setText(_translate("exportDialog", "..."))
        self.label_2.setText(_translate("exportDialog", "Output Folder"))
        self.label_3.setText(_translate("exportDialog", "Output File Prefix"))
        self.label.setText(_translate("exportDialog", "Export Results"))

