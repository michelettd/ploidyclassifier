#ifndef LOADFILESPARAM_H
#define LOADFILESPARAM_H

#include <QDialog>

namespace Ui {
class loadFilesParam;
}

class loadFilesParam : public QDialog
{
    Q_OBJECT

public:
    explicit loadFilesParam(QWidget *parent = 0);
    ~loadFilesParam();

private:
    Ui::loadFilesParam *ui;
};

#endif // LOADFILESPARAM_H
