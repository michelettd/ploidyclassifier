# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'triploid_class/errorsDialog.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_errorDialog(object):
    def setupUi(self, errorDialog):
        errorDialog.setObjectName("errorDialog")
        errorDialog.resize(432, 234)
        self.buttonBox = QtWidgets.QDialogButtonBox(errorDialog)
        self.buttonBox.setGeometry(QtCore.QRect(80, 200, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.errorConsole = QtWidgets.QTextEdit(errorDialog)
        self.errorConsole.setGeometry(QtCore.QRect(10, 10, 411, 171))
        self.errorConsole.setObjectName("errorConsole")

        self.retranslateUi(errorDialog)
        self.buttonBox.accepted.connect(errorDialog.accept)
        self.buttonBox.rejected.connect(errorDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(errorDialog)

    def retranslateUi(self, errorDialog):
        _translate = QtCore.QCoreApplication.translate
        errorDialog.setWindowTitle(_translate("errorDialog", "Error Message"))

