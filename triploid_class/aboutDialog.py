# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'triploid_class/aboutDialog.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_aboutDialog(object):
    def setupUi(self, aboutDialog):
        aboutDialog.setObjectName("aboutDialog")
        aboutDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        aboutDialog.resize(653, 562)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(aboutDialog.sizePolicy().hasHeightForWidth())
        aboutDialog.setSizePolicy(sizePolicy)
        aboutDialog.setMaximumSize(QtCore.QSize(726, 700))
        self.okButton = QtWidgets.QPushButton(aboutDialog)
        self.okButton.setGeometry(QtCore.QRect(510, 520, 122, 30))
        self.okButton.setObjectName("okButton")
        self.label = QtWidgets.QLabel(aboutDialog)
        self.label.setGeometry(QtCore.QRect(130, 10, 501, 21))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.image = QtWidgets.QLabel(aboutDialog)
        self.image.setGeometry(QtCore.QRect(10, 10, 131, 151))
        self.image.setText("")
        self.image.setPixmap(QtGui.QPixmap("assist.png"))
        self.image.setScaledContents(True)
        self.image.setObjectName("image")
        self.label_7 = QtWidgets.QLabel(aboutDialog)
        self.label_7.setGeometry(QtCore.QRect(250, 40, 101, 21))
        self.label_7.setObjectName("label_7")
        self.label_8 = QtWidgets.QLabel(aboutDialog)
        self.label_8.setGeometry(QtCore.QRect(150, 110, 471, 51))
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(aboutDialog)
        self.label_9.setGeometry(QtCore.QRect(51, 171, 542, 21))
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(aboutDialog)
        self.label_10.setGeometry(QtCore.QRect(70, 198, 565, 21))
        self.label_10.setObjectName("label_10")
        self.label_11 = QtWidgets.QLabel(aboutDialog)
        self.label_11.setGeometry(QtCore.QRect(70, 219, 561, 21))
        self.label_11.setObjectName("label_11")
        self.label_12 = QtWidgets.QLabel(aboutDialog)
        self.label_12.setGeometry(QtCore.QRect(70, 240, 348, 21))
        self.label_12.setObjectName("label_12")
        self.label_13 = QtWidgets.QLabel(aboutDialog)
        self.label_13.setGeometry(QtCore.QRect(50, 280, 571, 221))
        self.label_13.setTextFormat(QtCore.Qt.RichText)
        self.label_13.setOpenExternalLinks(True)
        self.label_13.setObjectName("label_13")

        self.retranslateUi(aboutDialog)
        self.okButton.clicked.connect(aboutDialog.accept)
        QtCore.QMetaObject.connectSlotsByName(aboutDialog)

    def retranslateUi(self, aboutDialog):
        _translate = QtCore.QCoreApplication.translate
        aboutDialog.setWindowTitle(_translate("aboutDialog", "Credits"))
        self.okButton.setText(_translate("aboutDialog", "OK"))
        self.label.setText(_translate("aboutDialog", "TriClass: Triploid classifier and genotyper"))
        self.label_7.setText(_translate("aboutDialog", "Version 1.00"))
        self.label_8.setText(_translate("aboutDialog", "blablablh"))
        self.label_9.setText(_translate("aboutDialog", "aaaaaa"))
        self.label_10.setText(_translate("aboutDialog", "a"))
        self.label_11.setText(_translate("aboutDialog", "a"))
        self.label_12.setText(_translate("aboutDialog", "a"))
        self.label_13.setText(_translate("aboutDialog", "<html><head/><body><p>For questions contacts Diego Micheletti at <a href=\"mailto:diego.micheletti@fmach.it\"><span style=\" text-decoration: underline; color:#0000ff;\">diego.micheletti@fmach.it</span></a></p><p><span style=\" font-family:\'arial,sans-serif\'; color:#222222;\"><br/>©Fondazione Edmund Mach (</span><a href=\"http://www.fmach.it\"><span style=\" text-decoration: underline; color:#0000ff;\">www.fmach.it</span></a>) - San Michele all\'Adige (TN) - Italy</p></body></html>"))

