#!/usr/local/bin/python3.9
#  -*- coding: utf-8 -*-
from __init__ import version
import os
import subprocess
import sys
from collections import Counter

# PyQt5
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog, QMainWindow, QMessageBox
from PyQt5.QtGui import QColor, QTextCursor

# My ui and modules
from triploid_class.loadfilesparam import Ui_loadFilesParam
from triploid_class.mainwindow import Ui_TriClass
from triploid_class.exportDialog import Ui_exportDialog
from triploid_class.aboutDialog import Ui_aboutDialog
from ploidyClassifierCMD import Exporter, GetDataFast, PloidyCalculator, TriploidRecall, TriploidPedigree, UiPlotter

try:
    from matplotlib.backends.backend_qt5agg import NavigationToolbar2QTAgg as NavigationToolbar
except ImportError:
    from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

f"""
 -- GUI to Identify and genotype triploid from Illumina infinium 

@author:     michelettd

@copyright:  2019 FEM. All rights reserved.

@license:    GPL

@version: {version}

@deffield    updated: Updated
"""

global log_file
log_file = open('triploid_caller.log', 'w+')


# Import the SNP_filter_pipeline
class InFilesParamDialog(QDialog, Ui_loadFilesParam):
    def __init__(self):
        super(InFilesParamDialog, self).__init__(None)
        self.ui = Ui_loadFilesParam()
        self.ui.setupUi(self)

        self.foldName = None

        # Connect radio buttons
        self.ui.precomputedArrButton.toggled.connect(lambda: self.btnstate(self.ui.precomputedArrButton, finrep=True))
        self.ui.pedigreeButton.toggled.connect(lambda: self.btnstate(self.ui.pedigreeButton))
        self.ui.aneuploidsButton.toggled.connect(lambda: self.btnstate(self.ui.aneuploidsButton))

        # Connect File selection
        self.ui.dataTableSelect.clicked.connect(lambda: self.open_file_chooser(self.ui.dataTableText,
                                                                               "All Files (*);;TXT files (*.txt)",
                                                                               flist=True))
        self.ui.snpSubsetSelect.clicked.connect(lambda: self.open_file_chooser(self.ui.snpSetText,
                                                                               "All Files (*);;TXT files (*.txt)"))
        self.ui.iidSubsetSelect.clicked.connect(lambda: self.open_file_chooser(self.ui.iidSetText,
                                                                               "All Files (*);;TXT files (*.txt)"))

        # Connect file prefix text box
        self.ui.outfolderSelect.clicked.connect(lambda: self.open_dir_chooser(self.ui.outfolderText, ""))
        self.ui.outfolderText.editingFinished.connect(lambda: self.check_input_data(self.ui.outfolderText,
                                                                                    "outputfolder"))

        start_folder = os.getcwd()
        self.ui.outfolderText.setText(start_folder)
        # Set Focus to enforce specifying outfolder
        self.ui.outfolderText.setFocus(True)
        self.ui.arbnameText.editingFinished.connect(lambda: self.check_input_data(self.ui.arbnameText,
                                                                                  "Temp. file prefix"))

    # Methods.

    def btnstate(self, b, finrep=False):
        if b.isChecked():
            if finrep:
                self.ui.finrepLabel.setEnabled(False)
                self.ui.dataTableSelect.setEnabled(False)
                self.ui.dataTableText.setEnabled(False)
            return True
        else:
            if finrep:
                self.ui.finrepLabel.setEnabled(True)
                self.ui.dataTableSelect.setEnabled(True)
                self.ui.dataTableText.setEnabled(True)
            return False

    def open_file_chooser(self, qtext, filetype, flist=False):
        if self.foldName is None:
            self.foldName = QtCore.QDir.homePath()
        if len(qtext.text()) > 0:
            self.foldName = qtext.text()
        if flist:
            mystr = QFileDialog.getOpenFileNames(self, "Choose file", self.foldName, filetype)
        else:
            mystr = QFileDialog.getOpenFileName(self, "Choose file", self.foldName, filetype)
        if isinstance(mystr, tuple):
            mystr = mystr[0]
        else:
            print(type(mystr))
        if mystr:
            if flist:
                qtext.setText(', '.join(mystr))
                self.foldName = mystr[0]
                self.ui.outfolderText.setText(os.path.split(mystr[0])[0])
            else:
                qtext.setText(mystr)
                self.foldName = mystr

    def open_dir_chooser(self, qtext, filetype):
        """rootDir = QtCore.QDir.homePath()
        if len(qtext.text()) > 0:
            rootDir = qtext.text()"""
        myDial = QFileDialog()
        myDial.setFileMode(2)
        mystr = myDial.getExistingDirectory(self, "Choose output directory")
        if mystr:
            qtext.setText(mystr)

    def check_input_data(self, what, data_label):
        val = str(what.text())
        if len(val) == 0:
            warn = QMessageBox()
            warn.setWindowTitle("Input Error: " + data_label)
            warn.setText("Warning " + data_label + " cannot be empty. Please specify a value.")
            warn.exec_()
        else:
            if data_label == "outputfolder":
                if not os.path.isdir(val) or (os.path.isdir(val) and not os.access(val, os.W_OK)):
                    try:
                        os.makedirs(val)
                    except:
                        warn = QMessageBox()
                        warn.setWindowTitle("Input Error: " + data_label)
                        if os.path.isdir(val):
                            warn.setText("Cannot WRITE to output folder " + val + ". Please specify a different one.")
                        else:
                            warn.setText("Cannot CREATE output folder " + val + ". Please specify a different one.")
                        warn.exec_()

                        self.open_dir_chooser(self.ui.outfolderText, "")

    def getFilesAndParams(self):
        res = dict()
        t = self.ui.dataTableText.text()
        res["finalreport"] = t  # .replace(' ', '').split(',')
        t = self.ui.snpSetText.text()
        res["snp_list"] = str(t)
        t = self.ui.iidSetText.text()
        res["iid_list"] = str(t)
        t = self.ui.outfolderText.text()
        res["tmp_dir"] = str(t)
        t = self.ui.arbnameText.text()
        res["ar_bname"] = str(t)
        t = self.ui.precomputedArrButton.isChecked()
        res["use_precomputed_arrays"] = bool(t)
        t = self.ui.pedigreeButton.isChecked()
        res["pedigree"] = bool(t)
        t = self.ui.aneuploidsButton.isChecked()
        res["aneuploids"] = bool(t)
        t = self.ui.threadsText.text()
        res["threads"] = int(t)

        return res


class exportDialog(QDialog, Ui_exportDialog):
    def __init__(self, startFolder):
        super(exportDialog, self).__init__(None)
        self.ui = Ui_exportDialog()
        self.ui.setupUi(self)
        # Connect buttons.
        self.ui.outfolderSelect.clicked.connect(lambda: self.openDirChooser(self.ui.outfolderText, ""))
        self.ui.outfolderText.editingFinished.connect(lambda: self.checkInputData(self.ui.outfolderText, "outputfolder"))
        self.ui.outfileText.editingFinished.connect(lambda: self.checkInputData(self.ui.outfileText, "outputfile"))
        self.ui.outfolderText.setText(startFolder)
        # Set Focus to enforce specifying outfolder
        self.ui.outfolderText.setFocus(True)

    def checkInputData(self, what, dataLabel):
        val = str(what.text())
        if len(val) == 0:
                warn = QMessageBox()
                warn.setWindowTitle("Input Error: " + dataLabel)
                warn.setText("Warning " + dataLabel + " cannot be empty. Please specify a value.")
                warn.exec_()
                if dataLabel == "outputfile":
                    self.ui.outfileText.setFocus(True)
        else:

            if dataLabel == "outputfolder":
                if not os.path.isdir(val) or (os.path.isdir(val) and not os.access(val, os.W_OK)):
                    try:
                        os.makedirs(val)
                    except:
                        warn=QMessageBox()
                        warn.setWindowTitle("Input Error: " + dataLabel)
                        if os.path.isdir(val):
                            warn.setText("Cannot WRITE to output folder " + val + ". Please specify a different one.")
                        else:
                            warn.setText("Cannot CREATE output folder " + val + ". Please specify a different one.")
                        warn.exec_()

                        self.openDirChooser(self.ui.outfolderText, "")

    def openDirChooser(self, qtext, filetype):
        rootDir = QtCore.QDir.homePath()
        if len(qtext.text()) > 0:
            rootDir = qtext.text()
        myDial = QFileDialog()
        myDial.setFileMode(2)
        mystr = myDial.getExistingDirectory(self, "Choose output directory")
        if str:
            qtext.setText(mystr)

                
class aboutDialog(QDialog, Ui_aboutDialog):
    def __init__(self):
        super(aboutDialog, self).__init__(None)
        self.ui = Ui_aboutDialog()
        self.ui.setupUi(self)
        # Connect buttons.


class programWindow(QMainWindow, Ui_TriClass):
    def __init__(self):
                    
        super(programWindow, self).__init__(None)
        # Set up the user interface from Designer.
        self.ui = Ui_TriClass()
        self.ui.setupUi(self)

        self.dialog = None
        self.params = None
        self.export = None
        self.about = None
        
        self.paramsArray = {}
        self.ui.consoleTextEdit.setText("Welcome to SNP Filter...\nStart Loading Input Files")
        
        # Connect buttons.
        self.ui.paramButton.clicked.connect(lambda: self.showSetParams())  # self.paramsArray))
        self.ui.runButton.clicked.connect(lambda: self.runProgram())  # self.paramsArray))
        self.ui.exportButton.clicked.connect(lambda: self.showExport())  # self.paramsArray))
        self.ui.iidfindButton.clicked.connect(lambda: self.finder(self.ui.indTableWidget, self.ui.iidText))
        self.ui.iidText.returnPressed.connect(lambda: self.finder(self.ui.indTableWidget, self.ui.iidText))
        self.ui.snpfindButton.clicked.connect(lambda: self.finder(self.ui.snpTableWidget, self.ui.snpidText))
        self.ui.snpidText.returnPressed.connect(lambda: self.finder(self.ui.snpTableWidget, self.ui.snpidText))
        # Connect actions within menu
        self.ui.actionSet_parameters_and_input.triggered.connect(lambda: self.showSetParams())  # self.paramsArray))
        self.ui.actionRun.triggered.connect(lambda: self.runProgram())  # self.paramsArray))
        self.ui.actionExport.triggered.connect(lambda: self.showExport())  # self.paramsArray))
        self.ui.actionAbout.triggered.connect(self.showAbout)
        """
        self.ui.actionReference_Manual.triggered.connect(self.showManual)
        """
        # Connect table selection change
        self.ui.indTableWidget.itemSelectionChanged.connect(lambda: self.plot_ball(\
             self.ui.indTableWidget.item(self.ui.indTableWidget.currentRow(), 0).text(),
             self.ui.indTableWidget.item(self.ui.indTableWidget.currentRow(), 1).text()))
        self.ui.snpTableWidget.itemSelectionChanged.connect(
            lambda: self.plot_snps(self.ui.snpTableWidget.item(self.ui.snpTableWidget.currentRow(), 0).text()))
        self.ui.progressBar.setVisible(True)
        self.ui.progressLabel.setVisible(True)
        mpl_toolbar = NavigationToolbar(self.ui.plotWidget.canvas, self)
        self.ui.gridLayout_2.addWidget(mpl_toolbar, 1, 0)
        self.ui.gridLayout.setAlignment(mpl_toolbar, QtCore.Qt.AlignCenter)

    # load set parameters  and file dialog
    def showSetParams(self):
        if self.dialog is None:
            self.dialog = InFilesParamDialog()
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"\nParamer setting...")
        res = self.dialog.exec_()
        self.inFilesArray = self.dialog.getFilesAndParams()
        errorMsg = ""
        outRes = self.inFilesArray
        # print(outRes)
        if outRes['ar_bname'] is False and len(outRes['finalreport']) == 0:
            errorMsg += "Final Report File cannot be empty when Use precomputed arrays is not selected\n"
        for el in ('finalreport', "snp_list", "iid_list"):
            for fname in [e.lstrip(' ') for e in outRes[el].split(',')]:
                if len(fname) > 0 and not os.path.exists(fname):
                    errorMsg += el + " file \'"+fname+"' does not exist!\n"
        
        if errorMsg == "":
            self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText() +
                                            " done.\nFiles and parameters looking OK.")
            self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText() + "\nAll set to RUN the analysis.")
            self.ui.runButton.setEnabled(True)
            self.ui.runLabel.setEnabled(True)
            self.ui.exportButton.setEnabled(False)
            self.ui.exportLabel.setEnabled(False)
            self.ui.actionRun.setEnabled(True)
            self.ui.actionExport.setEnabled(False)
            
        else:
            self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+" done.\n")
            cc = self.ui.consoleTextEdit.textColor()
            self.ui.consoleTextEdit.setTextColor(QColor("red"))
            self.ui.consoleTextEdit.append("ERRORS FOUND.\n"+errorMsg)
            self.ui.consoleTextEdit.setTextColor(cc)
            self.ui.consoleTextEdit.append("")
            self.ui.runButton.setEnabled(False)
            self.ui.runLabel.setEnabled(False)
            self.ui.actionRun.setEnabled(False)
            self.ui.exportButton.setEnabled(False)
            self.ui.exportLabel.setEnabled(False)
            self.ui.actionExport.setEnabled(False)
            
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)    
        
    # run program

    def runProgram(self):
        self.ui.progressBar.setValue(0)
        self.ui.progressBar.setVisible(True)
        self.ui.progressLabel.setVisible(True)
        self.ui.iidfindButton.setEnabled(False)
        self.ui.snpfindButton.setEnabled(False)
        
        # Clear Table and Summary when Run start
        self.ui.inFilesLabel_2.clear()
        self.ui.indTableWidget.clearContents()
        self.ui.snpTableWidget.clearContents()
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"\nRUNNING...\n")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.indTableWidget.setRowCount(0)
        self.ui.indTableWidget.setSortingEnabled(False)
        self.ui.snpTableWidget.setRowCount(0)
        self.ui.snpTableWidget.setSortingEnabled(False)

        # Let's run the pipeline
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"\n1 Getting Data...")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(1)
        if self.inFilesArray["snp_list"] != '':
            print(self.inFilesArray["snp_list"])
            self.snp_subset = set(l.strip().split()[0]
                                  for l in open(self.inFilesArray["snp_list"])
                                  if len(l.strip().split()) > 0)
        else:
            self.snp_subset = None

        self.inFilesArray["finalreport"] = self.inFilesArray["finalreport"].replace(' ', '').split(',')
        arraypath = os.path.join(self.inFilesArray['tmp_dir'], self.inFilesArray["ar_bname"])
        try:
            self.data = GetDataFast(use_precomputed_arrays=self.inFilesArray["use_precomputed_arrays"],
                                    ar_bname=arraypath,
                                    fnames=self.inFilesArray["finalreport"])
        except FileNotFoundError as err:
            self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText() +
                                            "\nError: %s" % str(err))
            return
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"done ")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(1)
        if self.inFilesArray["iid_list"] == 'debug':
            self.iid_set = ('1_17_james_grieve', '1_1_co-op_36', '1_7_fiamma')
        elif self.inFilesArray["iid_list"]:
            # print(self.inFilesArray["iid_list"])
            self.iid_set = tuple(l.strip().split()[0] for l in open(self.inFilesArray["iid_list"]) if len(l.strip().split()) > 0)
        else:
            self.iid_set = self.data.samples_order
        if not self.snp_subset:
            self.snp_subset = set(self.data.snp_order)

        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"\n2 Inferring ploidy...")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(2)

        # Calculate ploidy
        self.plclassif = PloidyCalculator(snp_order=self.data.snp_order,
                                          samples_order=self.data.samples_order,
                                          data_mat=self.data.data_mat,#self.data.het_ball,#
                                          het_count=self.data.het_count)

        #self.plclassif.ploidy = self.plclassif.get_ploidy_lmfit_multiproc(sample2parse=self.iid_set, snp2parse=self.snp_subset,
        #                                                        concurrency=self.inFilesArray["threads"])
        self.plclassif.ploidy = self.plclassif.get_ploidy_lmfit(sample2parse=self.iid_set,
                                        snp2parse=self.snp_subset)
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"done  ")    
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)               
        self.ui.progressBar.setValue(2)
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"\n3 Inferring triploid genotypes...")

        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(3)
        # Get triploid genotpe
        self.tr = TriploidRecall(ploidy=self.plclassif.ploidy,
                                 samples_order_dict=self.data.samples_order_dict,
                                 snp_order=self.data.snp_order,
                                 data_mat=self.data.data_mat,
                                 alleles=self.data.snp_alleles)
        self.tr.trigt, self.tr.trip_order = self.tr.iterator(snp_subset=self.snp_subset)

        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"done")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(3)
        # Get relationsips
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()
                                        + "\n4 Inferring Pedigrees (if enabled) ...")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(4)
        self.tp = TriploidPedigree(trip_order=self.tr.trip_order,
                                   trigt=self.tr.trigt,
                                   ploidy=self.plclassif.ploidy,
                                   samples_order_dict=self.data.samples_order_dict,
                                   data_mat=self.data.data_mat,
                                   snp_alleles=self.data.snp_alleles)

        if self.inFilesArray["pedigree"]:
            self.tp.relationship = self.tp.iterator(snp_order=self.data.snp_order,
                                                    snp2parse=self.snp_subset)
            # self.tp.relationship = self.tp.iterator(snp_order=self.data.snp_order, snp2parse=self.snp_subset)
        else:
            self.tp.relationship = dict()
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"done")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(4)
        par = 0
        for _, v in self.tp.relationship.items():
            if 'unreduced' in v:
                par += 1
            if 'dip_par' in v:
                par += 1
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"\n5 Populate Tables and Summary ...")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(5)
        # Populate Summary
        html_text = ('<p><b>Number of SNPs in the FinalReport:</b> ' + str(len(self.data.snp_order)) + '</p>' 
                     '<p><b>Number of SNPs considered:</b> ' + str(len(self.snp_subset)) + '</p>'
                     '<p><b>Number of Individuals:</b> ' + str(len(self.data.samples_order)) + '<p>'
                     '<p><b>Number of Individuals considered:</b> ' + str(len(self.iid_set)) + '</p>'                                                                          
                     '<p><b>Number of Diploid individuals:</b> ' + str(len([0 for _, v in self.plclassif.ploidy.items()
                                                                            if v == 2])) + '</p>'
                     '<p><b>Number of Triploid individuals:</b> ' + str(len([0 for _, v in self.plclassif.ploidy.items()
                                                                             if v == 3])) + '</p>'
                     '<p><b>Number of individuals with different ploidy:</b> '
                     + str(len([0 for _, v in self.plclassif.ploidy.items() if v != 3 and v != 2])) + '</p>'
                     '<p><b>Number of triploid individuals with potential parents\
                          in the FinalReport:</b> ' + str(par) + '</p>'
                     )
        self.ui.inFilesLabel_2.setText(html_text)
        # Populate ploidy Table 
        self.ui.indTableWidget.setColumnCount(4)
        self.ui.indTableWidget.setRowCount(len(self.plclassif.ploidy))
        self.populate_ploidy_table(self.plclassif.ploidy, self.tp.relationship)
        self.ui.indTableWidget.setSortingEnabled(True)
        
        # Populate SNP table
        self.ui.snpTableWidget.setColumnCount(6)
        self.ui.snpTableWidget.setRowCount(len(self.snp_subset))
        self.populate_snp_table(self.tr.trigt, self.data.snp_order)
        self.ui.snpTableWidget.setSortingEnabled(True)
        
        # Activate find and Export button
        self.ui.iidfindButton.setEnabled(True)
        self.ui.snpfindButton.setEnabled(True)
        self.ui.exportButton.setEnabled(True)
        self.ui.exportLabel.setEnabled(True)
        self.ui.actionExport.setEnabled(True)    
        self.ui.exportLabel.setEnabled(True)
        self.ui.runButton.setEnabled(False)
        self.ui.runLabel.setEnabled(False)
        self.ui.actionRun.setEnabled(False)
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText() + "done")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        self.ui.progressBar.setValue(5)

    def populate_snp_table(self, trigt, snp_order):
        for i, snp in enumerate(snp_order):
            col = 0  # SNPid
            self.writecell(self.ui.snpTableWidget, i, col, snp)
            gtcnt = Counter(trigt[:, i])
            if gtcnt['---'] == trigt.shape[0]:
                for col in range(1, 5):
                    self.writecell(self.ui.snpTableWidget, i, col, '0')
                self.writecell(self.ui.snpTableWidget, i, 5, str(gtcnt['---']))
            else:
                als = sorted(''.join((a for a in gtcnt if a != '---')))
                a1 = als[0]
                if als[0] != als[-1]:
                    a2 = als[-1]
                else:
                    a2 = ''
                col = 1 
                self.writecell(self.ui.snpTableWidget, i, col, str(gtcnt[a1*3]))
                col = 2
                self.writecell(self.ui.snpTableWidget, i, col, str(gtcnt[a1*2+a2]))
                col = 3
                self.writecell(self.ui.snpTableWidget, i, col, str(gtcnt[a1+a2*2]))
                col = 4
                self.writecell(self.ui.snpTableWidget, i, col, str(gtcnt[a2*3]))
                col = 5
                self.writecell(self.ui.snpTableWidget, i, col, str(gtcnt['---']))
                
    def populate_ploidy_table(self, ploidy, relationship):
        for ln, (k, v) in enumerate(sorted(ploidy.items(), key=lambda x: (x[1], x[0]), reverse=True)):
            # Individual ID
            self.writecell(self.ui.indTableWidget, ln, 0, k)
            # Ploidy
            self.writecell(self.ui.indTableWidget, ln, 1, v)
            # Unreduced gamete
            try:
                ured = str(len(relationship[k]['unreduced']))
            except KeyError:
                ured = '--'
            self.writecell(self.ui.indTableWidget, ln, 2, ured)
            # Normal gamete
            try:
                norm = str(len(relationship[k]['dip_par']))
            except KeyError:
                norm = '--'
            self.writecell(self.ui.indTableWidget, ln, 3, norm)
                
    def writecell(self, tabid, ln, c_num, value):
        item = QtWidgets.QTableWidgetItem()
        item.setData(Qt.DisplayRole, value)
        tabid.setItem(ln, c_num, item)
            
    # load export dialog
    def showExport(self):
        """
        if self.inFilesArray["output"]:
        """
        if self.export is None:
           
            startFold = self.inFilesArray['tmp_dir']
            self.export = exportDialog(startFold)

        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"\nEditing Export Parameters...")    
        res = self.export.exec_()
        self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText()+"done.")
        self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        if res == 1:
            outpath = os.path.join(self.export.ui.outfolderText.text(), self.export.ui.outfileText.text())
            ee = Exporter(fout_bname=outpath,
                          snp2parse=self.snp_subset,
                          ploidy=self.plclassif.ploidy,
                          trigt=self.tr.trigt,
                          trip_order=self.tr.trip_order,
                          snp_order=self.data.snp_order)
            ee.ploidy_classif(self.tp.relationship)
            ee.triploid_gts()

            self.ui.consoleTextEdit.setText(self.ui.consoleTextEdit.toPlainText() +
                                            "\nResults SUCCESSFULLY written to:\n  " +
                                            self.export.ui.outfolderText.text())
            self.ui.consoleTextEdit.moveCursor(QTextCursor.End)
        
    # load about dialog
    def showAbout(self):
        if self.about is None:
            self.about = aboutDialog()
        res = self.about.exec_()
        
    def plot_ball(self, iid, ploidy):
        plotter = UiPlotter(snp_order=self.data.snp_order,
                            samples_order=self.data.samples_order,
                            data_mat=self.data.data_mat)

        self.ui.plotWidget.canvas = plotter.plot_ploidy(iid=iid,
                                                        ploidy_iid=ploidy,
                                                        canvas=self.ui.plotWidget.canvas,
                                                        snp2parse=self.snp_subset)

    def plot_snps(self, snpid):
        plotter = UiPlotter(snp_order=self.data.snp_order,
                            samples_order=self.data.samples_order,
                            data_mat=self.data.data_mat)

        self.ui.plotWidget.canvas = plotter.plot_gslike(snpid=snpid,
                                                        canvas=self.ui.plotWidget.canvas,
                                                        trip_order=self.tr.trip_order,
                                                        trigt=self.tr.trigt,
                                                        snp_alleles=self.data.snp_alleles,
                                                        iid2parse=self.iid_set)

    def finder(self, table, val):
        snpid = val.text()
        found = table.findItems(snpid,QtCore.Qt.MatchContains)  # 2 == MatchContains
        # print found[p0].setSelected(True)
        if len(found) > 0:
            row = found[0].row()
            table.setCurrentItem(found[0])
            table.scrollToItem(found[0])
            table.setFocus()
            
    def showManual(self):
        filepath = os.path.join(os.curdir, 'docs', 'ASSIsT_Reference_Manual.pdf')
        if sys.platform.startswith('darwin'):
            subprocess.call(('open', filepath))
        elif os.name == 'nt':
            os.system("start "+filepath)
        elif os.name == 'posix':
            subprocess.call(('xdg-open', filepath))
        # webbrowser.open(os.path.join(os.curdir, 'docs','_build','html','index.html'))#'./docs/_build/html/index.html')


class CheckBoxItem(QtWidgets.QTableWidgetItem):
    def __lt__(self, other):
        if isinstance(other, QtGui.QTableWidgetItem):
            my_value = self.checkState()
            other_value = other.checkState()
            return my_value < other_value

        return super(CheckBoxItem, self).__lt__(other)


if __name__ == "__main__":
    # visualize the GUI
    app = QApplication(sys.argv)
    ui = programWindow()
    app.setWindowIcon(QtGui.QIcon('assist.ico'))
    ui.setWindowIcon(QtGui.QIcon('assist.ico'))
    ui.show()
    app.exec_()
