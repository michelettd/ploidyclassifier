# PloidyClassifier

## A tool for ploidy inference, and for genotyping and parental testing of triploids

**Authors:**   Diego Micheletti

**Version:** 1.0.0   

**ploidyClassifier** is a tool to classify the ploidy of samples
genotyped with Illumina Infinium arrays. Each individual can be
classified as di-, tri- or tetraploid and for triploid individuals the
genotype is recalculated taking into account the dosage. Triploid individuals
are tested for potential unreduced gamete-donating parents (UGDPs) and for
reduced gamete-donating parents (RGDPs).


# Availability

Source code are available for download [here](https://bitbucket.org/michelettd/ploidyclassifier/src/master/)

# Getting started

## Run ploidyClassifier

### Installing additional python modules trough pip 

The source code is a collection of python script, and they can be executed
from any operating system with python 3 installed. Before to run ploidyClassifier it is
necessary to install the following python modules:

- PyQt5
- NumPy
- matplotlib
- SciPy
- pandas
- lmfit
- openpyxl

It is possible to install all the required packages using pip as follows:  
`pip install -r requirements.txt` 


### Run the Graphical User Interface (GUI)

To open the graphical user interface in Unix (and in macOS) based OS open a shell terminal, 
navigate to the directory that contain the `ploidyClassifierGUI.py` file and run:
```bash
python3 ploidyClassifierGUI.py 
```

In Windows, open a command propt (cmd), navigate to the directory that contain  and
type: `ploidyClassifyGUI.py`, alternatively to the command prompt use the Linux Subsystem for
Windows and run as in Unix. 

![PloidyClassifier layout.](./docs/_images/mainWin.png)

The analysis can be performed by loading input files and parameters
using the `Set` button. Use the ![load](./docs/_images/loadfile.png) button to select the 
appropriate input file or enter the file name (with the full path) in the text box.

![Windows opened by clicking on the Set button](./docs/_images/loadfile_win.png)

The `Use precomputed array` switch allow to repeat an analysis, and it is intendend 
for debugging. To check the pedigree of the
triploid individuals in the dataset activate the `Check Pedigree` switch.
Using the `SNPs subset to use` and `Individuals subset to use` options it is possible to 
exclude from te analysis a part of the markers or individuals. The list of SNPs or 
indivisuals to analyse should be contained in a file with one entry per row. The use of a
subset of SNPs known to have a good clustering when looking at the values of Theta and R 
allows to have a more accurate determination of the ploidy level of the individuals. 

It is now possible run the analysis by clicking ![Run](./docs/_images/run.png).

At the end of the analysis it is possible to export the results by
clicking ![export dialog](./docs/_images/export.png). The output file is an
MS Excel file with 2 sheets, one with the ploidy of each individual and
one with the genotype of the triploid individuals.

On the bottom part of the GUI are present two tables; the one on the left
reports the information on the ploidy of each sample, and the
one on the right reports the result of the genotyping in the triploid
individuals.

The Plot Viewer widget shows the histogram of the B Allele frequency
when a cell of the ploidy table (left, 'Fig. B allele frequency') is selected, and it
shows the Genome Studio like plot of an SNP when a cell of the SNP table
(right, Fig. GenomeStudioLike) is selected.

![B allele frequency](./docs/_images/ploidy.png)
*Fig. B Allele frequency: The BAF histogram is represented in blue and 
the red line represent the best fit of mixture models with one, two or three 
gaussian components.*

![Plot of Theta vs R](./docs/_images/gslike.png)
*Fig. GenomeStudioLike: Picture of the R and Theta values for a specific marker. All the 
different genotypes are reported in different colors.*

### Using Docker (that is, on MS Windows, Mac OS X and Linux!)


> **Note: tested on Ubuntu 20.04.4 LTS with docker v.20.10.12**

The easiest way to run ploidyClassifier is through [Docker](https://www.docker.com/).
Docker works similarly to a virtual machine image, providing a container in
which all the software has already been installed, configured and tested.


> 1. Install Docker for [Linux](https://docs.docker.com/linux/),
>       [Mac OS X](https://docs.docker.com/mac/) or 
>       [Windows](https://docs.docker.com/windows/).
> 
> 2. Run the ``Docker Quickstart Terminal`` (Mac OS X, Windows) or the
>       ``docker`` daemon (Linux, ``sudo service docker start``).
> 3. Download the latest version:  
>      `docker pull michelettd/ploidyclassifier:1.0`
> 4. Run an instance of the image, mounting the host working directory
>       (e.g. ``/Users/diego/data``) on to the container working directory
>       ``/data``:  
>    ```bash
>    docker run --rm -e "DISPLAY=$DISPLAY" -v "$HOME/.Xauthority:/root/.Xauthority:ro" \
>           -v /Users/diego/data:/data  --network host  -it michelettd/ploidyclassifier:1.0
>    ```  
>    You need to write something like `-v //c/Users/diego/data:/data` if
>       you are in Windows or `-v /home/diego/data:/data` in Linux. The
>       ``--rm`` option automatically removes the container when it exits.
> 5. Now you can use the ploidyClassifier. To start the GUI run:    
>    ```shell
>    root@9ca8a19cd8cb:/# python3 /ploidyclassifier/ploidyClassifierGUI.py
>   ```


### Run without GUI

open a shell terminal, 
navigate to the directory that contain the `ploidyClassifierGUI.py` file and run:

`python3 ploidyClassifierCMD.py` 


    usage: ploidyClassifierCMD.py [-h] [-i INPUT [INPUT ...]] [-o OUTPUT] [-b AR_BNAME] [-u] [--plot] 
                              [-l SNP_LIST] [-s IID_LIST] [-p] [-t THREADS] [--version]
    optional arguments:
      -h, --help            show this help message and exit
      -i INPUT [INPUT ...], --input INPUT [INPUT ...]
                            Final report(s) from GenomeStudio (default: None)
      -o OUTPUT, --output OUTPUT
                            Output File basename (default: triplo_pippo)
      -b AR_BNAME, --ar_bname AR_BNAME
                            Basename to prepernd to the Numpy memorymap arrays and temp files 
                            (default: tmp)
      -u, --use_precomputed_arrays
                            Skip the array creation from final report(s) and get arrays names 
                            from ar_bname. (default: False)
      --plot                Create the plots with the distibution of the B allele frequency in
                            the "plots" directory. WARNING: One plot for each individual. (default: False)
      -l SNP_LIST, --snp-list SNP_LIST
                            File with the SNPs to use. One SNP per line (default: None)
      -s IID_LIST, --iid-list IID_LIST
                            File with the list of individuals to analyse. If iid-list is set to 
                            "debug" only 3 individuals are used and if it is not set (None) all 
                            the individuals are classiefied (default: None)
      -p, --pedigree        For each triploid individual check if any diploid accession is compatible
                            with parent-offspring relations (default: False)
      -t THREADS, --threads THREADS
                            number of threads to use [deprecated] (default: 3)
      --version             show the version number and exit



## Input File

The input file for ploidyClassifier is the Genome Studio Final Report produced as follows: 
Using the Report Wizard (Open GenomeStudio® → Analysis → Reports → Report wizard), select 
Final Report, and press Next. On the following page, use the “redo with the best 10th Percentile
GC Score” option, and press Next. If some samples have been excluded from the GenomeStudio ® 
project remove or zero-out them in the sample report. Chose how to account for excluded data and 
press Next (if no samples have been excluded from GenomeStudio ® project this page is not displayed). 
The format of the Final Report must be set to “Standard” (the other choices are “Matrix” or “3rd
Party”). The Final Report should include at least the following columns:
- SNP Name
- Sample ID
- Allele1 - Top (or Allele1-AB or Allele1-design, depending on the de-
sired type of output)
- Allele2 - Top (or Allele2-AB or Allele2-design, has to be consistent with
the choice for Allele1)
- Theta
- R
- B Allele Freq

Select Group by SNP (and not by “sample”). In General Option, select
“Tab” as the field delimiter. Press Next, and select the folder in which the
file has to be stored, and enter its name. Press Finish.
The Final report can be compressed (using gzip) and also split in multiple files to avoid 
huge file.

# How to cite

TODO: Inseert reference to paper

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


